import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Anagram {

/**
 * This method finds all anagrams from dictionary
 * @param dictionary
 * @return
 */
	
	public Map<Long,List<String>> findAllAnagramsInDictionary(Iterator<String> dictionary) {
		Map<Long,List<String>> anagramMap=null;
		List<String> anagramList=null;
		try {
			for (Iterator<String> element=dictionary ;dictionary.hasNext();) {
				String word=element.next();
				if(anagramMap == null)
					anagramMap=new HashMap<Long,List<String>>();
				long code=anagramCode(word);
				if(anagramMap.containsKey(code)){
					anagramList=anagramMap.get(code);
					anagramList.add(word);
				}else{
					anagramList=new LinkedList<String>();
					anagramList.add(word);
				}
				anagramMap.put(code, anagramList);
			}
		} catch (Exception e) {
		}
		return anagramMap;
	}
	
	
	/*ss the DataType, returned from findAllAnagramsInDictionary, into this method, in order to find all anagrams for a specific word.*/
	List<String> findAllAnagramsForWord(String word, Map<Long,List<String>> anagramMap) {
		List<String> anagramList=null;
		try {
			if((word !=null && word.length() > 0) && (anagramMap !=null && anagramMap.size() > 0)){
				anagramList = anagramMap.get(anagramCode(word));
			}
		} catch (Exception e) {
		}
		return anagramList;
	}
	
	private long anagramCode(String word){
		try {
			char[] c=word.toCharArray();
			if( c.length ==0)
				return map.get(c[0]);
			long temp=1;
			for (int i = 0; i < c.length; i++) {
				temp*=map.get(c[i]);
			}
			return temp*word.length();
		} catch (Exception e) {
			
		}
		return 0;
	}

	Map<Character,Integer> map=new HashMap<Character,Integer>();{
		map.put('a', 2);
		map.put('b', 3);
		map.put('c', 5);
		map.put('d', 7);
		map.put('e', 11);
		map.put('f', 13);
		map.put('g', 17);
		map.put('h', 19);
		map.put('i', 23);
		map.put('j', 29);
		map.put('k', 31);
		map.put('l', 37);
		map.put('m', 41);
		
		
		map.put('n', 43);
		map.put('o', 47);
		map.put('p', 53);
		map.put('q', 59);
		map.put('r', 61);
		map.put('s', 67);
		map.put('t', 71);
		map.put('u', 73);
		map.put('v', 79);
		map.put('w', 83);
		map.put('x', 89);
		map.put('y', 97);
		map.put('z', 101);
		
	}
	

}
